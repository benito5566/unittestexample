﻿namespace CalculatorProject
{
    public class CalculatorWithLogger
    {
        public CalculatorWithLogger()
        {

        }

        public int Sum(int a, int b)
        {
            var result = a + b;

            new Logger().Log($"Sum result: {result}");

            return result;
        }
    }
}
