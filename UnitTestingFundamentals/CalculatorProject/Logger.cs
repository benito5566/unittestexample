﻿using System;

namespace CalculatorProject
{
    public interface ILogger
    {
        void Log(string message);
    }
    public class Logger
    {
        public void Log(string message)
        {
            // this requires a console, but it can be anything like a file in the system, an email being sent, 
            // a web service over which we don't have control.
            throw new Exception();
            //Console.WriteLine(message);
        }
    }
}
