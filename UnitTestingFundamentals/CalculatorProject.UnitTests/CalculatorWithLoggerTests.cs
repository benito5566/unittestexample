﻿using NUnit.Framework;

namespace CalculatorProject.UnitTests
{
    [TestFixture]
    public class CalculatorWithLoggerTests
    {
        [Test]
        public void Sum_NumberPlusZero_ReturnsNum()
        {
            var sut = new CalculatorWithLogger();
            const int Zero = 0;

            var result = sut.Sum(5, Zero);

            Assert.That(result, Is.EqualTo(5));
        }
    }
}
