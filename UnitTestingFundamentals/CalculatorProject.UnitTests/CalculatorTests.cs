﻿using NUnit.Framework;

namespace CalculatorProject.UnitTests
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void Sum_NumPlusZero_ReturnsNum()
        {
            // Arrange
            var sut = new Calculator();
            const int Zero = 0;

            // Act
            var result = sut.Sum(3, Zero);

            // Assert
            Assert.That(result, Is.EqualTo(3));
        }
    }
}
